<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <style type="text/css">
div.sourceCode { overflow-x: auto; }
table.sourceCode, tr.sourceCode, td.lineNumbers, td.sourceCode {
  margin: 0; padding: 0; vertical-align: baseline; border: none; }
table.sourceCode { width: 100%; line-height: 100%; }
td.lineNumbers { text-align: right; padding-right: 4px; padding-left: 4px; color: #aaaaaa; border-right: 1px solid #aaaaaa; }
td.sourceCode { padding-left: 5px; }
code > span.kw { color: #007020; font-weight: bold; } /* Keyword */
code > span.dt { color: #902000; } /* DataType */
code > span.dv { color: #40a070; } /* DecVal */
code > span.bn { color: #40a070; } /* BaseN */
code > span.fl { color: #40a070; } /* Float */
code > span.ch { color: #4070a0; } /* Char */
code > span.st { color: #4070a0; } /* String */
code > span.co { color: #60a0b0; font-style: italic; } /* Comment */
code > span.ot { color: #007020; } /* Other */
code > span.al { color: #ff0000; font-weight: bold; } /* Alert */
code > span.fu { color: #06287e; } /* Function */
code > span.er { color: #ff0000; font-weight: bold; } /* Error */
code > span.wa { color: #60a0b0; font-weight: bold; font-style: italic; } /* Warning */
code > span.cn { color: #880000; } /* Constant */
code > span.sc { color: #4070a0; } /* SpecialChar */
code > span.vs { color: #4070a0; } /* VerbatimString */
code > span.ss { color: #bb6688; } /* SpecialString */
code > span.im { } /* Import */
code > span.va { color: #19177c; } /* Variable */
code > span.cf { color: #007020; font-weight: bold; } /* ControlFlow */
code > span.op { color: #666666; } /* Operator */
code > span.bu { } /* BuiltIn */
code > span.ex { } /* Extension */
code > span.pp { color: #bc7a00; } /* Preprocessor */
code > span.at { color: #7d9029; } /* Attribute */
code > span.do { color: #ba2121; font-style: italic; } /* Documentation */
code > span.an { color: #60a0b0; font-weight: bold; font-style: italic; } /* Annotation */
code > span.cv { color: #60a0b0; font-weight: bold; font-style: italic; } /* CommentVar */
code > span.in { color: #60a0b0; font-weight: bold; font-style: italic; } /* Information */
  </style>
  <link rel="stylesheet" href="markdown4.css" type="text/css" />
</head>
<body>
<h1 id="using-cif-dictionaries-with-pycifrw">Using CIF dictionaries with PyCIFRW</h1>
<h2 id="introduction">Introduction</h2>
<p>CIF dictionaries describe the meaning of the datanames found in CIF data files in a machine-readable format - the CIF format. Each block in a CIF dictionary defines a single dataname by assigning values to a limited set of attributes. This set of attributes used by a dictionary is called its 'Dictionary Definition Language' or DDL. Three languages have been used in IUCr-supported CIF dictionaries: DDL1 (the original language), DDL2 (heavily developed by the macromolecular community), and DDLm (a new standard that aims to unite the best of DDL1 and DDL2). DDL2 and DDLm both allow algorithms to be defined for datanames. These algorithms describe how to derive values for datanames from other quantities in the data file.</p>
<p>Knowing the dictionary that a given datafile is written with reference to thus allows us to do two things: to <em>validate</em> that datanames and values match the constraints imposed by the definition; and, in the case of DDL2 and DDLm, to <em>calculate</em> values which might then be used for checking or simply to fill in missing information.</p>
<h2 id="dictionaries">Dictionaries</h2>
<p>DDL dictionaries can be read into <code>CifFile</code> objects just like CIF data files. For this purpose, <code>CifFile</code> objects automatically support save frames (used in DDL2 and DDLm dictionaries), which are accessed just like <code>CifBlock</code>s using their save frame name. By default save frames are not listed as keys in <code>CifFile</code>s as they do not form part of the CIF standard.</p>
<p>The more powerful <code>CifDic</code> object creats a unified interface to DDL1, DDL2 and DDLm dictionaries. A <code>CifDic</code> is initialised with a single file name or <code>CifFile</code> object, and will accept the grammar keyword:</p>
<div class="sourceCode"><pre class="sourceCode python"><code class="sourceCode python">    cd <span class="op">=</span> CifFile.CifDic(<span class="st">&quot;cif_core.dic&quot;</span>,grammar<span class="op">=</span><span class="st">&#39;1.1&#39;</span>)</code></pre></div>
<p>Definitions are accessed using the usual notation, e.g. <code>cd['_atom_site_aniso_label']</code>. Return values are always <code>CifBlock</code> objects. Additionally, the <code>CifDic</code> object contains a number of instance variables derived from dictionary global data:</p>
<dl>
<dt><code>dicname</code></dt>
<dd>The dictionary name + version as given in the dictionary
</dd>
<dt><code>diclang</code></dt>
<dd>'DDL1','DDL2', or 'DDLm'
</dd>
</dl>
<p><code>CifDic</code> objects provide a large number of validation functions, which all return a Python dictionary which contains at least the key <code>result</code>. <code>result</code> takes the values <code>True</code>, <code>False</code> or <code>None</code> depending on the success, failure or non-applicability of each test. In case of failure, additional keys are returned depending on the nature of the error.</p>
<h2 id="validation-with-pycifrw">Validation with PyCIFRW</h2>
<p>A top level function is provided for convenient validation of CIF files:</p>
<div class="sourceCode"><pre class="sourceCode python"><code class="sourceCode python">    CifFile.Validate(<span class="st">&quot;mycif.cif&quot;</span>,dic <span class="op">=</span> <span class="st">&quot;cif_core.dic&quot;</span>)</code></pre></div>
<p>This returns a tuple <code>(valid_result, no_matches)</code>. <code>valid_result</code> and <code>no_matches</code> are Python dictionaries indexed by block name. For <code>valid_result</code>, the value for each block is itself a dictionary indexed by item_name. The value attached to each item name is a list of <code>(check_function, check_result)</code> tuples, with <code>check_result</code> a small dictionary containing at least the key <code>result</code>. All tests which passed or were not applicable are removed from this dictionary, so <code>result</code> is always <code>False</code>. Additional keys contain auxiliary information depending on the test. Each of the items in <code>no_matches</code> is a simple list of item names which were not found in the dictionary.</p>
<p>If a simple validation report is required, the function <code>validate_report</code> can be called on the output of the above function, printing a simple ASCII report. This function can be studied as an example of how to process the structure returned by the <code>validate</code> function.</p>
<p>A somewhat nicer interface to validation is provided in the <code>ValidationResult</code> class (thanks to Boris Dusek), which is initialised with the return value from validate:</p>
<div class="sourceCode"><pre class="sourceCode python"><code class="sourceCode python">    val_report <span class="op">=</span> ValidationResult(validate(<span class="st">&quot;mycif.cif&quot;</span>,dic<span class="op">=</span><span class="st">&quot;cif_core.dic&quot;</span>))</code></pre></div>
<p>This class provides the <code>report</code> method, producing a human-readable report, as well as boolean methods which return whether or not the block is valid or if items appear in the block that are not present in the dictionary - <code>is_valid</code> and <code>has_no_match_items</code> respectively.</p>
<h3 id="limitations-on-validation">Limitations on validation</h3>
<ol style="list-style-type: decimal">
<li><p>(DDL2 only) When validating data dictionaries themselves, no checks are made on group and subgroup consistency (e.g. that a specified subgroup is actually defined).</p></li>
<li><p>(DDL1 only) Some <code>_type_construct</code> attributes in the DDL1 spec file are not machine-readable, so values cannot be checked for consistency</p></li>
<li><p>DDLm validation methods are still in development so are not comprehensive.</p></li>
</ol>
</body>
</html>
